import { Test, TestingModule } from '@nestjs/testing';
<<<<<<< HEAD
import { title } from 'process';
=======
>>>>>>> b93dd72 (Add unit test for create book)
import { BooksController } from './books.controller';
import { BooksService } from './books.service';

describe('BooksController', () => {
  let controller: BooksController;

  const mockBooksService = {
    createBook: jest.fn((dto) => {
      return {
        ...dto,
        id: 'test_str',
        createdAt: 'test-created',
        updatedAt: 'test-updated',
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BooksController],
      providers: [BooksService],
    })
      .overrideProvider(BooksService)
      .useValue(mockBooksService)
      .compile();

    controller = module.get<BooksController>(BooksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create book', () => {
    const dto = {
      title: 'Test Title',
      author: 'Test Author',
    };

    expect(controller.post(dto)).toEqual({
      id: expect.any(String),
      createdAt: expect.any(String),
      updatedAt: expect.any(String),
      ...dto,
    });

    expect(mockBooksService.createBook).toHaveBeenCalledWith(dto);
  });
});
